
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Intervention implements Serializable {
	
	@Id
	private int idIntervention;
	private Date dateIntervention;
	private String heure ;
	
	public Intervention() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getHeure() {
		return heure;
	}
	public void setHeure(String heure) {
		this.heure = heure;
	}
	public Date getDateIntervention() {
		return dateIntervention;
	}
	public void setDateIntervention(Date dateIntervention) {
		this.dateIntervention = dateIntervention;
	}
	public int getIdIntervention() {
		return idIntervention;
	}
	public void setIdIntervention(int idIntervention) {
		this.idIntervention = idIntervention;
	}
	


}
