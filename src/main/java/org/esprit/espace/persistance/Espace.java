import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;


@Entity
public class Espace implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@GeneratedValue
	@Id
	private int idEsapce;
	private String nomEspace;
	@Enumerated
	private  Type type;
	private int capacite;
	@OneToMany(mappedBy = "espace")
	private List<InterventionPlanifiee> interventionplanifie;
	
	private Reservation reservation;
	
	public Espace() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdEsapce() {
		return idEsapce;
	}
	public void setIdEsapce(int idEsapce) {
		this.idEsapce = idEsapce;
	}
	public String getNomEspace() {
		return nomEspace;
	}
	public void setNomEspace(String nomEspace) {
		this.nomEspace = nomEspace;
	}
	
	public int getCapacite() {
		return capacite;
	}
	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
	public List<InterventionPlanifiee> getInterventionplanifie() {
		return interventionplanifie;
	}
	public void setInterventionplanifiee(List<InterventionPlanifiee> interventionplanifie) {
		this.interventionplanifie = interventionplanifie;
	}
	

}
