

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class InterventionPlanifiee implements Serializable {
	
	@Id
	private int idIntp;
	 
	
	private Date date;
	private String heure ;
	private String description;
	
	@ManyToOne
	private Espace espace;
	


	public int getIdIntp() {
		return idIntp;
	}


	public void setIdIntp(int idIntp) {
		this.idIntp = idIntp;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getHeure() {
		return heure;
	}


	public void setHeure(String heure) {
		this.heure = heure;
	}


	public Espace getEspace() {
		return espace;
	}


	public void setEspace(Espace espace) {
		this.espace = espace;
	}

    public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


}